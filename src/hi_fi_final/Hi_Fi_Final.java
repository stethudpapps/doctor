/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hi_fi_final;

import static hi_fi_final.tcp_reading.mul;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.text.DecimalFormat;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.style.markers.SeriesMarkers;


/**
 *
 * @author Aravind B
 * version 2.2
 * This Application is designed to run on PI.  
 */
public class Hi_Fi_Final {
    public static Thread tcpThread;
    public static double time=0;
    public static int Winframe =0;
    public static boolean QUIT_APP = false;
    
    public static String message = "Welcome";
    
    public static DecimalFormat df = new DecimalFormat("#.##");


    /**
     * @param args the command line arguments
     */

    public static PipedOutputStream aus_output = new PipedOutputStream();
    public static PipedInputStream aus_input;
     
    
    public static void main(String[] args) throws Exception{

        aus_input = new PipedInputStream(aus_output,15*8000);//15 secs circular buffer enabled
        aus_output.flush();
        
        int ViewWindowtimeInSec = 2;
        double[] xarray = new double[ViewWindowtimeInSec*4000];
        double[] yarray = new double[ViewWindowtimeInSec*4000];
        double temp=0; 
        
       
        for(int a=0;a<ViewWindowtimeInSec*4000;++a)
        {
            xarray[a]= a;
            //temp += (1.0/4000);
            yarray[a]=0;
        }    
        
        XYChart chart = new XYChartBuilder().width(800).height(380).title("Auscultation").xAxisTitle("Time in mins").yAxisTitle("Amplitude (V)").build();
        XYSeries series = chart.addSeries("signal", xarray, yarray);
        series.setMarker(SeriesMarkers.NONE);
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Line);
        chart.getStyler().setLegendVisible(false);
        chart.getStyler().setMarkerSize(0);
        chart.getStyler().setYAxisMax(3.0);
        chart.getStyler().setYAxisMin(-3.0);

    
        JPanel chartPanel = new XChartPanel<XYChart>(chart);
        JLabel label = new JLabel("..", SwingConstants.CENTER);
        JButton jb = new JButton("EXIT APPLICATION");
        JLabel labelInfo = new JLabel("TS", SwingConstants.CENTER);
        labelInfo.setFont(new Font("Serif", Font.BOLD, 14));
        //JLabel labelAmplitude = new JLabel(" Amplitude : ", SwingConstants.CENTER);
        JLabel labelVolume = new JLabel("Volume ", SwingConstants.CENTER);
        JComboBox cb = new JComboBox();
        cb.addItem("1x");
        cb.addItem("2x");
        cb.addItem("3x");
        JPanel southPanel = new JPanel();
        JPanel northPanel = new JPanel();
        
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame frame = new JFrame("TELE-STETHOSCOPE");
                frame.setLayout(new BorderLayout(10,10));
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setUndecorated(true);

                frame.add(chartPanel, BorderLayout.CENTER);
                frame.add(label, BorderLayout.NORTH);
                
                northPanel.add(labelInfo);
                //northPanel.add(labelHRate);
                frame.add(northPanel,BorderLayout.NORTH);
                
                southPanel.add(labelVolume);
                southPanel.add(cb);
                southPanel.add(jb);
                frame.add(southPanel,BorderLayout.SOUTH);  
                
                tcp_reading.mul = 1;
                cb.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        tcp_reading.mul = cb.getSelectedIndex()+1;
                    }
                });

//                jb.setBounds(50,100,95,30);
//                jb.setPreferredSize(new Dimension(100, 50));
                jb.addActionListener(new ActionListener(){ 

                    @Override
                    public void actionPerformed(ActionEvent e) {
                         tcp_reading.TCP_Thread_Run = false;
                         QUIT_APP = true;
                         System.exit(0);
                      
                    }
//                        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    });
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setLocation(0, 0);
                frame.setVisible(true);
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        
        tcp_reading tcp = new tcp_reading();
        tcpThread = new Thread(tcp);
        tcpThread.start(); // This will try to connect to the server and start reading from the socket
        
        Winframe = ViewWindowtimeInSec;
        int framexindex=0;
        time=0;
        
        for(int a=0;a<ViewWindowtimeInSec*4000;++a)
                {
                    xarray[a]= time;
                    time += (1.0/(4000*60));  
                }
      
      chart.getStyler().setXAxisMax(time);
      chart.getStyler().setXAxisMin(0.0);
      
    int cur_d_len =0;
    byte[] temp_array = new byte[2];
    int dyn_index=0;
    time =0;
    double temp_value=0.0;
      
        while (!QUIT_APP) { 
            
            
        cur_d_len = aus_input.available();
        
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

                    @Override
                        public void run() {
                            //labelInfo.setText("hai");
                            labelInfo.setText(message);
                            }
            });
    
        if(cur_d_len >10) 
        {
            //cur_d_len = aus_data.size();
            for(dyn_index=0;dyn_index+(cur_d_len/2)< yarray.length;++dyn_index)
            {
                yarray[dyn_index] = yarray[dyn_index+(cur_d_len/2)];
                xarray[dyn_index] = xarray[dyn_index+(cur_d_len/2)];
                
            }
            //--dyn_index;
            for(int a=dyn_index;a<xarray.length;++a)
            {
                xarray[a] = time; 
                time += (1.0/(4000*60));
            }
            
            for(int b=0;(b+2)<cur_d_len;b+=2) 
            {
      
                aus_input.read(temp_array,0,2);
                temp_value = ((temp_array[1] << 8) | (temp_array[0] & 0x0FF));
                yarray[dyn_index] = (temp_value*mul*3)/10000; 
                //xarray[dyn_index] = time; 
                ++dyn_index;
          
            }
           
            chart.getStyler().setXAxisMax(time-(1.0/(4000*60)));
            //chart.getStyler().setXAxisMax(xarray[xarray.length-1]);
            chart.getStyler().setXAxisMin(xarray[0]);
            
            javax.swing.SwingUtilities.invokeLater(new Runnable() {

                    @Override
                        public void run() {
                            //labelInfo.setText("hai");
                            //labelInfo.setText(message);
                            chart.updateXYSeries("signal", xarray, yarray, null);
                            chartPanel.revalidate();
                            chartPanel.repaint();
                            }
            });
            
        }
        //else Thread.sleep(50);
            
            Thread.sleep(750);
            
        }
        
        //label.setText("Application Stopped");
        aus_input.close();
        aus_output.close();
       
    }
    public static double getMax(double [] inputArray) { 
        double maxValue = inputArray[0]; 
        for(int i=1;i < inputArray.length;i++){ 
          if(inputArray[i] > maxValue){ 
             maxValue = inputArray[i];
          } 
        } 
        return Double.parseDouble(df.format(maxValue));
    }
    
    public static int getHR() {
        return 72;
    }
    
}
